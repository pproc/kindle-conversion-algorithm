/**
 * Created by Piotr Proc on 2017-01-01.
 */

var submitButton = document.getElementById("submitButton"),
    inputTextarea = document.getElementById("inputTextarea"),
    outputTextarea = document.getElementById("outputTextarea"),
    regExpTextarea = document.getElementById("regExpTextarea");

submitButton.addEventListener("click", function (event) {
    event.preventDefault();
    var inputText = inputTextarea.value,
        regExps = JSON.parse(regExpTextarea.value),
        reformattedText = reformater.reformatText(inputText, regExps);

    outputTextarea.value = reformattedText;
    fileDownloader.download(reformattedText, "book.txt");
});


