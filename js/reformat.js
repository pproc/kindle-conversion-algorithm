/**
 * Created by Piotr Proc on 2017-01-01.
 */

var reformater = {
    reformatText: function(outputText, regExps) {
        for (var i = 0; i < regExps.length; i++) {
            var regExp = regExps[i];
            outputText = outputText.replaceAll(new RegExp(regExp.from), regExp.to);
        }

        return outputText;
    }
};
