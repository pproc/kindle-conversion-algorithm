__author__ = 'Piotr Proc'

import re

LOCALIZATION = "E:\Piotrek\workspaces\my_mini_projects\kindle_format_algorithm/"
FILE_NAME = 'wozinski'
EXTENSION = '.txt'

file = LOCALIZATION + FILE_NAME + EXTENSION
new_file = LOCALIZATION + FILE_NAME + "_new" + EXTENSION

a = "a"
b = "b"
c = "c"
d = "d"
e = "e"

with open(file, 'r') as f:
    output = f.read()
    new_output = ""

    for index in range(0, len(output), 50):
        start = index
        end = start + 50

        fragment = output[start:end]
        fragment = re.sub(r"(\.|\?|!)\s?\n(.)", r"\1\n\n\2", fragment, re.MULTILINE)
        # if previous line ends with . or ? or ! - we add one more line breaking character
        fragment = re.sub(r"(.) \n(.)", r"\1 \2", fragment, re.MULTILINE)
        #if next two words are in two lines (and they should be in one) we delete line breaking character
        fragment = re.sub(r"\s([A-Z]{2,})\s", r"\n\1: ", fragment, re.MULTILINE)
        # if we have dialog we insert BreakLine                                     //DIALOGS
        fragment = re.sub(r"(\.|\?|!)\s?\n(.)", r"\1\n\n\2", fragment, re.MULTILINE)
        # if previous line ends with . or ? or ! - we add one more line breaking character  //DIALOGS
        new_output += fragment

    # print(new_output)

    with open(new_file, "w+") as text_file:  #w+ means that file will be created if doesn't exists
        text_file.write(new_output)